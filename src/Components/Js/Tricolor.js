import React, { Component } from 'react'

import "../Css/Tricolor.css";

export default class Tricolor extends React.Component {
    constructor(props) {
        super(props);
        this.state = { alerta: 0 };
        this.changeAlert =
            this.changeAlert.bind(this);

    }
    changeAlert() {
        this.setState({
            alerta: this.state.alerta + 1
        })
    }
    render() {
        let ColorSelected;
        if (this.state.alerta === 4) {
            this.setState({
                alerta: 0
            })
        }
        switch (this.state.alerta) {
            case 0:
                ColorSelected = "grey";
                break;
            case 1:
                ColorSelected = "red";
                break;
            case 2:
                ColorSelected = "green";
                break;
            case 3:
                ColorSelected = "blue";
                break;
        }
        return (
            <div className="Circulo" onClick={this.changeAlert} style={{ backgroundColor: ColorSelected }} />

        )
    }
}

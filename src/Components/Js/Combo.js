import React, { Component } from 'react'
import { CIUTATS_CAT_20K } from '../../datos';

import "../Css/Combo.css";

export default class Combo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Comarca: '' , Pueblo: ''};
        this.SetComarca = this.SetComarca.bind(this);
        this.SetPueblo = this.SetPueblo.bind(this);
    }

    SetComarca(event) {
        this.setState({ Comarca: event.target.value });
    }

    SetPueblo(event)
    {
        this.setState({ Pueblo: event.target.value });
    }

    render() {
        let ListaComarcas = [];
        CIUTATS_CAT_20K.forEach(element => {
            let Comarca = element.comarca;
            let repetido = false;
            for (let i = 0; i < ListaComarcas.length; i++) {
                if (Comarca === ListaComarcas[i]) {
                    repetido = true;
                }
            }
            if (!repetido) {
                ListaComarcas.push(Comarca);
            }
        });
        let ListaComarcasParaOption = ListaComarcas.map(el => <option key={el}>{el}</option>);
        
        let ListaPueblos = [];
        CIUTATS_CAT_20K.forEach(element => {
            let Pueblo = element.municipi;
            let Comarca = element.comarca;
            let repetido = false;
            for (let i = 0; i < ListaPueblos.length; i++) {
                if (Pueblo === ListaPueblos[i] && Comarca === this.state.Comarca) {
                    repetido = true;
                }
            }
            if (!repetido && Comarca === this.state.Comarca) {
                ListaPueblos.push(Pueblo);
            }
        });
        let ListaPueblosParaOption = ListaPueblos.map(el => <option key={el}>{el}</option>);
        return (
            <>
                <h3>{this.state.Pueblo}</h3>
                <select onChange={this.SetComarca}>
                    {ListaComarcasParaOption}
                </select>
                <select onChange={this.SetPueblo}>
                    {ListaPueblosParaOption}
                </select>
            </>
        )
    }
}
import React from "react";

import Thumbs from "./Components/Js/Thumbs";
import Tricolor from "./Components/Js/Tricolor";
import Fotos from "./Components/Js/Fotos";
import Lista from "./Components/Js/Lista";
import Combo from "./Components/Js/Combo";

export default () => (
  <>
    <Thumbs />
    <Tricolor />
    <Fotos />
    <Lista />
    <Combo />
  </>
);
